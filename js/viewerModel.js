var wixViewModel = {
    "siteAssetsTestModuleVersion": "1.334.0",
    "requestUrl": "https:\/\/andreayjuanra.wixsite.com\/nuestraboda",
    "siteFeatures": [
        "assetsLoader",
        "businessLogger",
        "captcha",
        "commonConfig",
        "componentsLoader",
        "componentsRegistry",
        "consentPolicy",
        "dashboardWixCodeSdk",
        "environment",
        "locationWixCodeSdk",
        "navigationManager",
        "navigationPhases",
        "ooi",
        "pages",
        "renderer",
        "reporter",
        "router",
        "scrollRestoration",
        "seoWixCodeSdk",
        "seo",
        "sessionManager",
        "siteMembersWixCodeSdk",
        "siteMembers",
        "siteScrollBlocker",
        "siteWixCodeSdk",
        "stores",
        "structureApi",
        "thunderboltInitializer",
        "tpaCommons",
        "translations",
        "warmupData",
        "windowMessageRegistrar",
        "windowWixCodeSdk",
        "wixEmbedsApi",
        "componentsReact",
        "platform"
    ],
    "site": {
        "metaSiteId": "c8c0aea0-02a2-4cec-b797-48ff289b51dd",
        "userId": "7124a860-2eef-41f1-9ff5-671349e48f2a",
        "siteId": "d07130ce-edf0-4b82-a22f-cfb3d3414bd0",
        "externalBaseUrl": "https:\/\/andreayjuanra.wixsite.com\/nuestraboda",
        "siteRevision": 109,
        "siteType": "UGC",
        "dc": "uw2-pub-1",
        "isResponsive": false,
        "sessionId": "03df2cd0-93ad-484b-bda3-8dd27fde01bb"
    },
    "isMobileDevice": false,
    "viewMode": "desktop",
    "formFactor": "desktop",
    "deviceInfo": {
        "deviceClass": "Desktop"
    },
    "media": {
        "staticMediaUrl": "https:\/\/static.wixstatic.com\/media",
        "mediaRootUrl": "https:\/\/static.wixstatic.com\/",
        "staticVideoUrl": "https:\/\/video.wixstatic.com\/"
    },
    "language": {
        "userLanguage": "es",
        "userLanguageResolutionMethod": "QueryParam",
        "siteLanguage": "es",
        "isMultilingualEnabled": false,
        "directionByLanguage": "ltr"
    },
    "mode": {
        "qa": false,
        "enableTestApi": false,
        "debug": false,
        "ssrIndicator": false,
        "ssrOnly": false,
        "editorElementsVersion": "1.9440.0",
        "siteAssetsFallback": "enable"
    },
    "siteFeaturesConfigs": {
        "assetsLoader": {},
        "componentsRegistry": {
            "librariesTopology": [
                {
                    "artifactId": "editor-elements",
                    "namespace": "wixui",
                    "url": "https:\/\/static.parastorage.com\/services\/editor-elements\/1.9440.0"
                },
                {
                    "artifactId": "editor-elements",
                    "namespace": "dsgnsys",
                    "url": "https:\/\/static.parastorage.com\/services\/editor-elements\/1.9440.0"
                }
            ]
        },
        "consentPolicy": {
            "isWixSite": false
        },
        "dashboardWixCodeSdk": {},
        "dataWixCodeSdk": {
            "gridAppId": "de3bc19c-add4-41a0-ac6f-8e46f4fd2eaf",
            "segment": "LIVE"
        },
        "environment": {
            "editorType": "",
            "domain": "wixsite.com"
        },
        "fedopsWixCodeSdk": {
            "isWixSite": false
        },
        "locationWixCodeSdk": {
            "urlMappings": null
        },
        "onloadCompsBehaviors": {},
        "ooiTpaSharedConfig": {
            "imageSpriteUrl": "https:\/\/static.parastorage.com\/services\/santa-resources\/resources\/viewer\/editorUI\/fonts.v19.png",
            "wixStaticFontsLinks": [
                "https:\/\/static.parastorage.com\/services\/santa-resources\/resources\/viewer\/user-site-fonts\/v16\/languages-woff2.css"
            ]
        },
        "ooi": {
            "ooiComponentsData": {
                "14d2abc2-5350-6322-487d-8c16ff833c8a": {
                    "sentryDsn": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748",
                    "componentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/details-pageViewerWidgetNoCss.bundle.min.js",
                    "widgetId": "14d2abc2-5350-6322-487d-8c16ff833c8a",
                    "noCssComponentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/details-pageViewerWidgetNoCss.bundle.min.js",
                    "staticBaseUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0",
                    "isLoadable": true,
                    "isTPACssVars": false,
                    "isModuleFederated": false
                },
                "1440e92d-47d8-69be-ade7-e6de40127106": {
                    "sentryDsn": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748",
                    "componentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/widgetViewerWidgetNoCss.bundle.min.js",
                    "widgetId": "1440e92d-47d8-69be-ade7-e6de40127106",
                    "noCssComponentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/widgetViewerWidgetNoCss.bundle.min.js",
                    "staticBaseUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0",
                    "isLoadable": false,
                    "isTPACssVars": false,
                    "isModuleFederated": false
                },
                "405eb115-a694-4e2b-abaa-e4762808bb93": {
                    "sentryDsn": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748",
                    "componentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/members-pageViewerWidget.bundle.min.js",
                    "widgetId": "405eb115-a694-4e2b-abaa-e4762808bb93",
                    "noCssComponentUrl": "",
                    "staticBaseUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0",
                    "isLoadable": false,
                    "isTPACssVars": false,
                    "isModuleFederated": false
                },
                "29ad290c-8529-4204-8fcf-41ef46e0d3b0": {
                    "sentryDsn": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748",
                    "componentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/scheduleViewerWidgetNoCss.bundle.min.js",
                    "widgetId": "29ad290c-8529-4204-8fcf-41ef46e0d3b0",
                    "noCssComponentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/scheduleViewerWidgetNoCss.bundle.min.js",
                    "staticBaseUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0",
                    "isLoadable": false,
                    "isTPACssVars": false,
                    "isModuleFederated": false
                }
            },
            "viewMode": "Site",
            "formFactor": "Desktop",
            "blogMobileComponentUrl": "undefinedfeed-page-mobile-viewer.bundle.min.js"
        },
        "reporter": {
            "userId": "7124a860-2eef-41f1-9ff5-671349e48f2a",
            "metaSiteId": "c8c0aea0-02a2-4cec-b797-48ff289b51dd",
            "isPremium": false,
            "isFBServerEventsAppProvisioned": false,
            "dynamicPagesIds": []
        },
        "router": {
            "baseUrl": "https:\/\/andreayjuanra.wixsite.com\/nuestraboda",
            "mainPageId": "c1dmp",
            "pagesMap": {
                "v7e3t": {
                    "pageId": "v7e3t",
                    "title": "Detalles y registro del evento",
                    "pageUriSEO": "events",
                    "pageJsonFileName": "7124a8_a8a7321381a414eb057304249f995668_54"
                },
                "c1dmp": {
                    "pageId": "c1dmp",
                    "title": "ANDREA Y JUANRA",
                    "pageUriSEO": "andrea-y-juanra",
                    "pageJsonFileName": "7124a8_203e279d4025c48b4bc127b1171c21cf_109"
                },
                "warpj": {
                    "pageId": "warpj",
                    "title": "Agenda",
                    "pageUriSEO": "agenda",
                    "pageJsonFileName": "7124a8_e3130bce56f4404bce7192350f2160a5_88"
                }
            },
            "routes": {
                ".\/events": {
                    "type": "Static",
                    "pageId": "v7e3t"
                },
                ".\/andrea-y-juanra": {
                    "type": "Static",
                    "pageId": "c1dmp"
                },
                ".\/agenda": {
                    "type": "Static",
                    "pageId": "warpj"
                },
                ".\/": {
                    "type": "Static",
                    "pageId": "c1dmp"
                }
            },
            "isWixSite": false
        },
        "searchWixCodeSdk": {
            "language": "es"
        },
        "seo": {
            "context": {
                "siteName": "nuestraboda",
                "siteUrl": "https:\/\/andreayjuanra.wixsite.com\/nuestraboda",
                "indexSite": true,
                "defaultUrl": "https:\/\/andreayjuanra.wixsite.com\/nuestraboda",
                "currLangIsOriginal": true,
                "homePageTitle": "ANDREA Y JUANRA",
                "ogType": "article",
                "experiments": {
                    "specs.seo.UseMultilingualFilter": "true",
                    "specs.seo.ReverseResolveIsIndexableLogic": "true",
                    "specs.seo.EnableSiteSearchSD": "true",
                    "specs.seo.AddPaginationToTitle": "true",
                    "specs.seo.NewHreflangLogic": "true",
                    "specs.promote-seo.stop-calling-eureka-search-analytics-performance": "true",
                    "specs.seo.EnableLocalBusinessSD": "true",
                    "specs.seo.EnableFaqSD": "false",
                    "specs.promote-seo.deprecate-seo-manager-get-intent": "true",
                    "specs.promote-seo.stop-calling-legacy-get-profile": "true"
                }
            },
            "metaTags": [
                {
                    "name": "fb_admins_meta_tag",
                    "value": "",
                    "property": false
                }
            ],
            "customHeadTags": "",
            "isInSEO": false,
            "hasBlogAmp": false,
            "mainPageId": "c1dmp"
        },
        "sessionManager": {
            "dynamicModelApiUrl": "https:\/\/andreayjuanra.wixsite.com\/nuestraboda\/_api\/v2\/dynamicmodel",
            "expiryTimeoutOverride": 0
        },
        "siteMembersWixCodeSdk": {
            "isPreviewMode": false,
            "isEditMode": false,
            "smToken": "",
            "smcollectionId": "837e82b7-c0d3-454c-9462-e7e03c0103e9"
        },
        "siteMembers": {
            "collectionExposure": "Public",
            "smcollectionId": "837e82b7-c0d3-454c-9462-e7e03c0103e9",
            "smToken": "",
            "protectedHomepage": false,
            "isTemplate": false,
            "isCommunityInstalled": false
        },
        "siteWixCodeSdk": {
            "siteRevision": 109,
            "language": "es",
            "mainPageId": "c1dmp",
            "appsData": {
                "4": {
                    "appDefinitionId": "14d7032a-0a65-5270-cca7-30f599708fed"
                },
                "5": {
                    "appDefinitionId": "14bca956-e09f-f4d6-14d7-466cb3f09103"
                },
                "6": {
                    "appDefinitionId": "141fbfae-511e-6817-c9f0-48993a7547d1"
                },
                "7": {
                    "appDefinitionId": "13ee94c1-b635-8505-3391-97919052c16f"
                },
                "8": {
                    "appDefinitionId": "150ae7ee-c74a-eecd-d3d7-2112895b988a"
                },
                "9": {
                    "appDefinitionId": "a322993b-2c74-426f-bbb8-444db73d0d1b"
                },
                "10": {
                    "appDefinitionId": "55cd9036-36bb-480b-8ddc-afda3cb2eb8d"
                },
                "11": {
                    "appDefinitionId": "f123e8f1-4350-4c9b-b269-04adfadda977"
                },
                "12": {
                    "appDefinitionId": "9bead16f-1c73-4cda-b6c4-28cff46988db"
                },
                "13": {},
                "14": {
                    "appDefinitionId": "1480c568-5cbd-9392-5604-1148f5faffa0"
                },
                "15": {
                    "appDefinitionId": "13aa9735-aa50-4bdb-877c-0bb46804bd71"
                },
                "16": {
                    "appDefinitionId": "e3118e0a-b1c1-4e1d-b67d-ddf0cb92309b"
                },
                "18": {
                    "appDefinitionId": "14b89688-9b25-5214-d1cb-a3fb9683618b"
                },
                "19": {
                    "appDefinitionId": "135c3d92-0fea-1f9d-2ba5-2a1dfb04297e"
                },
                "20": {
                    "appDefinitionId": "146c0d71-352e-4464-9a03-2e868aabe7b9"
                },
                "21": {
                    "appDefinitionId": "139ef4fa-c108-8f9a-c7be-d5f492a2c939"
                },
                "22": {
                    "appDefinitionId": "ea2821fc-7d97-40a9-9f75-772f29178430"
                },
                "24": {
                    "appDefinitionId": "307ba931-689c-4b55-bb1d-6a382bad9222"
                },
                "25": {
                    "appDefinitionId": "d70b68e2-8d77-4e0c-9c00-c292d6e0025e"
                },
                "26": {
                    "appDefinitionId": "4b10fcce-732d-4be3-9d46-801d271acda9"
                },
                "27": {
                    "appDefinitionId": "14ce1214-b278-a7e4-1373-00cebd1bef7c"
                },
                "28": {
                    "appDefinitionId": "8725b255-2aa2-4a53-b76d-7d3c363aaeea"
                },
                "29": {
                    "appDefinitionId": "8ea9df15-9ff6-4acf-bbb8-8d3a69ae5841"
                },
                "1969": {
                    "appDefinitionId": "140603ad-af8d-84a5-2c80-a0f60cb47351"
                },
                "2256": {
                    "appDefinitionId": "675bbcef-18d8-41f5-800e-131ec9e08762"
                },
                "-666": {
                    "appDefinitionId": "22bef345-3c5b-4c18-b782-74d4085112ff"
                }
            },
            "pageIdToPrefix": {},
            "routerPrefixes": {},
            "pageIdToTitle": {
                "v7e3t": "Detalles y registro del evento",
                "c1dmp": "ANDREA Y JUANRA",
                "warpj": "Agenda"
            },
            "urlMappings": null,
            "viewMode": "Site"
        },
        "tinyMenu": {
            "languages": false
        },
        "tpaCommons": {
            "widgetsClientSpecMapData": {
                "141995eb-c700-8487-6366-a482f7432e2b": {
                    "widgetUrl": "https:\/\/so-feed.codev.wixapps.net\/widget",
                    "mobileUrl": "https:\/\/so-feed.codev.wixapps.net\/widget",
                    "tpaWidgetId": "shoutout_feed",
                    "appPage": {},
                    "applicationId": 19,
                    "appDefinitionName": "Email Marketing",
                    "appDefinitionId": "135c3d92-0fea-1f9d-2ba5-2a1dfb04297e",
                    "isWixTPA": true,
                    "allowScrolling": false
                },
                "14d2abc2-5350-6322-487d-8c16ff833c8a": {
                    "widgetUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/details-page",
                    "mobileUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/details-page",
                    "appPage": {
                        "id": "events",
                        "name": "Event Details",
                        "defaultPage": "",
                        "hidden": true,
                        "multiInstanceEnabled": false,
                        "order": 1,
                        "indexable": true,
                        "fullPage": false,
                        "landingPageInMobile": false,
                        "hideFromMenu": false
                    },
                    "applicationId": 1969,
                    "appDefinitionName": "Wix Events",
                    "appDefinitionId": "140603ad-af8d-84a5-2c80-a0f60cb47351",
                    "isWixTPA": true,
                    "allowScrolling": false
                },
                "1440e92d-47d8-69be-ade7-e6de40127106": {
                    "widgetUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/widget",
                    "mobileUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/widget",
                    "tpaWidgetId": "wix_events",
                    "appPage": {},
                    "applicationId": 1969,
                    "appDefinitionName": "Wix Events",
                    "appDefinitionId": "140603ad-af8d-84a5-2c80-a0f60cb47351",
                    "isWixTPA": true,
                    "allowScrolling": false
                },
                "405eb115-a694-4e2b-abaa-e4762808bb93": {
                    "widgetUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/members-page",
                    "mobileUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/members-page",
                    "appPage": {
                        "id": "events_members_page",
                        "name": "Events",
                        "defaultPage": "",
                        "hidden": true,
                        "multiInstanceEnabled": false,
                        "order": 2,
                        "indexable": true,
                        "fullPage": false,
                        "landingPageInMobile": false,
                        "hideFromMenu": false
                    },
                    "applicationId": 1969,
                    "appDefinitionName": "Wix Events",
                    "appDefinitionId": "140603ad-af8d-84a5-2c80-a0f60cb47351",
                    "isWixTPA": true,
                    "allowScrolling": false
                },
                "29ad290c-8529-4204-8fcf-41ef46e0d3b0": {
                    "widgetUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/schedule",
                    "mobileUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/schedule",
                    "tpaWidgetId": "agenda-page",
                    "appPage": {
                        "id": "Schedule",
                        "name": "Schedule",
                        "defaultPage": "",
                        "hidden": true,
                        "multiInstanceEnabled": false,
                        "order": 1,
                        "indexable": true,
                        "fullPage": false,
                        "landingPageInMobile": false,
                        "hideFromMenu": false
                    },
                    "applicationId": 1969,
                    "appDefinitionName": "Wix Events",
                    "appDefinitionId": "140603ad-af8d-84a5-2c80-a0f60cb47351",
                    "isWixTPA": true,
                    "allowScrolling": false
                }
            },
            "appsClientSpecMapByApplicationId": {
                "19": {
                    "widgets": {
                        "141995eb-c700-8487-6366-a482f7432e2b": {
                            "widgetUrl": "https:\/\/so-feed.codev.wixapps.net\/widget",
                            "widgetId": "141995eb-c700-8487-6366-a482f7432e2b",
                            "refreshOnWidthChange": true,
                            "mobileUrl": "https:\/\/so-feed.codev.wixapps.net\/widget",
                            "published": true,
                            "mobilePublished": true,
                            "seoEnabled": true,
                            "preFetch": false,
                            "shouldBeStretchedByDefault": false,
                            "shouldBeStretchedByDefaultMobile": false,
                            "componentFields": {},
                            "tpaWidgetId": "shoutout_feed",
                            "default": true
                        }
                    },
                    "applicationId": 19,
                    "appDefinitionName": "Email Marketing"
                },
                "1969": {
                    "widgets": {
                        "14d2abc2-5350-6322-487d-8c16ff833c8a": {
                            "widgetUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/details-page",
                            "widgetId": "14d2abc2-5350-6322-487d-8c16ff833c8a",
                            "refreshOnWidthChange": true,
                            "mobileUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/details-page",
                            "appPage": {
                                "id": "events",
                                "name": "Event Details",
                                "defaultPage": "",
                                "hidden": true,
                                "multiInstanceEnabled": false,
                                "order": 1,
                                "indexable": true,
                                "fullPage": false,
                                "landingPageInMobile": false,
                                "hideFromMenu": false
                            },
                            "published": true,
                            "mobilePublished": true,
                            "seoEnabled": true,
                            "preFetch": false,
                            "shouldBeStretchedByDefault": true,
                            "shouldBeStretchedByDefaultMobile": false,
                            "componentFields": {
                                "useSsrSeo": true,
                                "controllerUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/details-pageController.bundle.min.js",
                                "subPages": [
                                    {
                                        "key": "wix.events.sub_pages.event",
                                        "enumerable": true
                                    }
                                ],
                                "componentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/details-pageViewerWidget.bundle.min.js",
                                "viewer": {
                                    "errorReporting": {
                                        "url": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748"
                                    }
                                },
                                "isLoadable": true,
                                "ooiInEditor": true,
                                "noCssComponentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/details-pageViewerWidgetNoCss.bundle.min.js"
                            },
                            "default": false
                        },
                        "1440e92d-47d8-69be-ade7-e6de40127106": {
                            "widgetUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/widget",
                            "widgetId": "1440e92d-47d8-69be-ade7-e6de40127106",
                            "refreshOnWidthChange": true,
                            "mobileUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/widget",
                            "published": true,
                            "mobilePublished": true,
                            "seoEnabled": true,
                            "preFetch": false,
                            "shouldBeStretchedByDefault": false,
                            "shouldBeStretchedByDefaultMobile": false,
                            "componentFields": {
                                "useSsrSeo": true,
                                "componentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/widgetViewerWidget.bundle.min.js",
                                "controllerUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/widgetController.bundle.min.js",
                                "viewer": {
                                    "errorReporting": {
                                        "url": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748"
                                    }
                                },
                                "mobileHelpId": "a0621ef7-79ef-4a32-a376-1258506a5d2a",
                                "mobileSettingsEnabled": true,
                                "ooiInEditor": true,
                                "noCssComponentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/widgetViewerWidgetNoCss.bundle.min.js"
                            },
                            "tpaWidgetId": "wix_events",
                            "default": true
                        },
                        "405eb115-a694-4e2b-abaa-e4762808bb93": {
                            "widgetUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/members-page",
                            "widgetId": "405eb115-a694-4e2b-abaa-e4762808bb93",
                            "refreshOnWidthChange": true,
                            "mobileUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/members-page",
                            "appPage": {
                                "id": "events_members_page",
                                "name": "Events",
                                "defaultPage": "",
                                "hidden": true,
                                "multiInstanceEnabled": false,
                                "order": 2,
                                "indexable": true,
                                "fullPage": false,
                                "landingPageInMobile": false,
                                "hideFromMenu": false
                            },
                            "published": true,
                            "mobilePublished": true,
                            "seoEnabled": false,
                            "preFetch": false,
                            "shouldBeStretchedByDefault": false,
                            "shouldBeStretchedByDefaultMobile": true,
                            "componentFields": {
                                "componentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/members-pageViewerWidget.bundle.min.js",
                                "controllerUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/members-pageController.bundle.min.js",
                                "viewer": {
                                    "errorReporting": {
                                        "url": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748"
                                    }
                                },
                                "ooiInEditor": true,
                                "noCssComponentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/members-pageViewerWidgetNoCss.bundle.min.js"
                            },
                            "default": false
                        },
                        "29ad290c-8529-4204-8fcf-41ef46e0d3b0": {
                            "widgetUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/schedule",
                            "widgetId": "29ad290c-8529-4204-8fcf-41ef46e0d3b0",
                            "refreshOnWidthChange": true,
                            "mobileUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/schedule",
                            "appPage": {
                                "id": "Schedule",
                                "name": "Schedule",
                                "defaultPage": "",
                                "hidden": true,
                                "multiInstanceEnabled": false,
                                "order": 1,
                                "indexable": true,
                                "fullPage": false,
                                "landingPageInMobile": false,
                                "hideFromMenu": false
                            },
                            "published": true,
                            "mobilePublished": true,
                            "seoEnabled": false,
                            "shouldBeStretchedByDefault": true,
                            "shouldBeStretchedByDefaultMobile": true,
                            "componentFields": {
                                "controllerUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/scheduleController.bundle.min.js",
                                "componentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/scheduleViewerWidget.bundle.min.js",
                                "viewer": {
                                    "errorReporting": {
                                        "url": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748"
                                    }
                                },
                                "ooiInEditor": true,
                                "noCssComponentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/scheduleViewerWidgetNoCss.bundle.min.js"
                            },
                            "tpaWidgetId": "agenda-page",
                            "default": false
                        }
                    },
                    "applicationId": 1969,
                    "appDefinitionName": "Wix Events"
                }
            },
            "appsClientSpecMapData": {
                "135c3d92-0fea-1f9d-2ba5-2a1dfb04297e": {
                    "applicationId": 19,
                    "appDefinitionName": "Email Marketing",
                    "appFields": {
                        "premiumBundle": {
                            "parentAppId": "ee21fe60-48c5-45e9-95f4-6ca8f9b1c9d9",
                            "parentAppSlug": "ee21fe60-48c5-45e9-95f4-6ca8f9b1c9d9"
                        },
                        "isStandalone": true,
                        "semanticVersion": "^0.5.0"
                    },
                    "isWixTPA": true
                },
                "140603ad-af8d-84a5-2c80-a0f60cb47351": {
                    "applicationId": 1969,
                    "appDefinitionName": "Wix Events",
                    "appFields": {
                        "platform": {
                            "baseUrls": {
                                "baseUrl": "https:\/\/events.wixapps.net\/_api\/wix-one-events-server",
                                "staticsBaseUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0"
                            },
                            "editorScriptUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/editorScript.bundle.min.js",
                            "viewerScriptUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/viewerScript.bundle.min.js",
                            "baseUrlsTemplate": {
                                "staticsBaseUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0"
                            },
                            "margins": {
                                "desktop": {
                                    "top": {},
                                    "right": {},
                                    "bottom": {},
                                    "left": {}
                                },
                                "tablet": {
                                    "top": {},
                                    "right": {},
                                    "bottom": {},
                                    "left": {}
                                },
                                "mobile": {
                                    "top": {},
                                    "right": {},
                                    "bottom": {},
                                    "left": {}
                                }
                            },
                            "height": {
                                "desktop": {},
                                "tablet": {},
                                "mobile": {}
                            },
                            "isStretched": {},
                            "docking": {
                                "desktop": {},
                                "tablet": {},
                                "mobile": {}
                            },
                            "errorReporting": {
                                "url": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748"
                            },
                            "width": {
                                "desktop": {},
                                "tablet": {},
                                "mobile": {}
                            },
                            "viewer": {
                                "errorReporting": {
                                    "url": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748"
                                }
                            },
                            "ooiInEditor": true
                        },
                        "appConfig": {
                            "siteConfig": {
                                "siteStructureApi": "wixArtifactId:com.wixpress.wix-events-web"
                            },
                            "namespace": "wix.events"
                        },
                        "isStandalone": true,
                        "semanticVersion": "^0.727.0"
                    },
                    "isWixTPA": true
                }
            },
            "previewMode": false,
            "siteRevision": 109,
            "userFileDomainUrl": "filesusr.com",
            "metaSiteId": "c8c0aea0-02a2-4cec-b797-48ff289b51dd",
            "isPremiumDomain": false,
            "experiments": {
                "specs.thunderbolt.WixCodeAddGlobalsAmdDependency": true,
                "storeSanpshotOnRedis": true,
                "specs.thunderbolt.remove_comp_events_navigation_guard": true,
                "specs.thunderbolt.responsiveAbsoluteChildrenPosition": true,
                "specs.thunderbolt.WixCodeTelemetryNewEndpoint": true,
                "specs.thunderbolt.exposeSendSetPasswordInVelo": true,
                "specs.thunderbolt.dataCancelLinkPreventDefaultRemoval": true,
                "specs.thunderbolt.WixCodeSupportGenerateWebMethodUrlOverride": true,
                "specs.wixData.DynamicPagesRouter": true,
                "specs.thunderbolt.newLoginFlowOnProtectedCollection": true,
                "specs.thunderbolt.increasePlatformBiEventsExposure": true,
                "specs.thunderbolt.containersList": true,
                "specs.thunderbolt.byRefV2": true,
                "specs.thunderbolt.DatePickerPortal": true,
                "specs.thunderbolt.WixCodeAsyncDispatcher": true,
                "specs.thunderbolt.SearchBoxSuggestionsFacelift": true,
                "dm_migrateToTextTheme": true,
                "specs.thunderbolt.TagsCustomConfig": true,
                "specs.thunderbolt.loadHighQualityImagesAboveFold": true,
                "specs.thunderbolt.rejectApprovalNeededAuthenticationRequests": true,
                "specs.thunderbolt.shouldLoadGoogleSdkEarly": true,
                "specs.thunderbolt.checkIOSToAvoidBeacon": true,
                "specs.thunderbolt.loadFirstNFonts": "3",
                "specs.thunderbolt.veloWixMembersAmbassadorV2": true,
                "specs.thunderbolt.exposeNewDashboardSdkWithVelo": true,
                "specs.thunderbolt.blocksPlatformSDKViewer": true,
                "specs.thunderbolt.wixCodePrefetch": true,
                "specs.router.ShouldSupportMultilingualBySubdirectory": true,
                "specs.thunderbolt.allow_windows_reduced_motion": true,
                "specs.thunderbolt.siteAssetsPrefetch": true,
                "displayWixAdsNewVersion": true,
                "specs.thunderbolt.smModalsShouldWaitForAppDidMount": true,
                "specs.thunderbolt.ooi_css_optimization": true,
                "specs.thunderbolt.veloWixMembers": true,
                "specs.ShouldForceCaptchaVerificationOnSignupSpec": "Enabled",
                "specs.thunderbolt.FilterResponsiveEditorMasterPageTpas": true,
                "specs.thunderbolt.enableClientSideRenderTrailingHeader": true,
                "specs.thunderbolt.HangingSignupForNeededApprovalFlow": true,
                "specs.thunderbolt.fetchPriority": true,
                "specs.thunderbolt.maskImageCSS": true,
                "specs.thunderbolt.WixDataNamespace": true,
                "specs.thunderbolt.newAuthorizedPagesFlow": true,
                "specs.thunderbolt.chat_landing_page": true,
                "specs.thunderbolt.resourceFetcherConformToFetchApi": true
            },
            "routersConfig": {},
            "routersByPrefix": {},
            "viewMode": "site",
            "editorOrSite": "site",
            "externalBaseUrl": "https:\/\/andreayjuanra.wixsite.com\/nuestraboda",
            "tpaModalConfig": {
                "wixTPAs": {
                    "4": true,
                    "5": true,
                    "6": true,
                    "7": true,
                    "8": true,
                    "9": true,
                    "10": true,
                    "11": true,
                    "12": true,
                    "14": true,
                    "15": true,
                    "16": true,
                    "18": true,
                    "19": true,
                    "20": true,
                    "21": true,
                    "22": true,
                    "24": true,
                    "25": true,
                    "26": true,
                    "27": true,
                    "28": true,
                    "29": true,
                    "1969": true
                }
            },
            "appSectionParams": {},
            "requestUrl": "https:\/\/andreayjuanra.wixsite.com\/nuestraboda",
            "isMobileView": false,
            "deviceType": "desktop",
            "isMobileDevice": false,
            "extras": {},
            "tpaDebugParams": {
                "debugApp": null,
                "petri_ovr": null
            },
            "locale": "es",
            "shouldRenderTPAsIframe": true,
            "debug": false
        },
        "windowWixCodeSdk": {
            "isMobileFriendly": true,
            "formFactor": "Desktop",
            "pageIdToRouterAppDefinitionId": {}
        },
        "wixEmbedsApi": {
            "isAdminPage": false
        },
        "platform": {
            "landingPageId": "c1dmp",
            "isChancePlatformOnLandingPage": true,
            "clientWorkerUrl": "https:\/\/static.parastorage.com\/services\/wix-thunderbolt\/dist\/clientWorker.2e13135d.bundle.min.js",
            "bootstrapData": {
                "isMobileView": false,
                "appsSpecData": {
                    "14ce1214-b278-a7e4-1373-00cebd1bef7c": {
                        "appDefinitionId": "14ce1214-b278-a7e4-1373-00cebd1bef7c",
                        "type": "public",
                        "instanceId": "47fbd92d-8538-4b9f-9344-3122f084a4e2",
                        "appDefinitionName": "Wix Forms & Payments",
                        "isWixTPA": true,
                        "isModuleFederated": false
                    },
                    "140603ad-af8d-84a5-2c80-a0f60cb47351": {
                        "appDefinitionId": "140603ad-af8d-84a5-2c80-a0f60cb47351",
                        "type": "public",
                        "instanceId": "39e9c88b-70ea-4c4e-9e16-71ef1e50e959",
                        "appDefinitionName": "Wix Events",
                        "isWixTPA": true,
                        "isModuleFederated": false
                    },
                    "675bbcef-18d8-41f5-800e-131ec9e08762": {
                        "appDefinitionId": "675bbcef-18d8-41f5-800e-131ec9e08762",
                        "type": "siteextension",
                        "instanceId": "c8c540f1-f9df-45ef-aaa6-fe6b24361508",
                        "isModuleFederated": false
                    },
                    "dataBinding": {
                        "appDefinitionId": "dataBinding",
                        "type": "application",
                        "instanceId": "c8c540f1-f9df-45ef-aaa6-fe6b24361508",
                        "appDefinitionName": "Data Binding",
                        "isWixTPA": true,
                        "isModuleFederated": false
                    }
                },
                "appsUrlData": {
                    "14ce1214-b278-a7e4-1373-00cebd1bef7c": {
                        "appDefId": "14ce1214-b278-a7e4-1373-00cebd1bef7c",
                        "appDefName": "Wix Forms & Payments",
                        "viewerScriptUrl": "https:\/\/static.parastorage.com\/services\/forms-viewer\/1.773.0\/viewer-app.bundle.min.js",
                        "baseUrls": {
                            "": ""
                        },
                        "widgets": {}
                    },
                    "140603ad-af8d-84a5-2c80-a0f60cb47351": {
                        "appDefId": "140603ad-af8d-84a5-2c80-a0f60cb47351",
                        "appDefName": "Wix Events",
                        "viewerScriptUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/viewerScript.bundle.min.js",
                        "baseUrls": {
                            "baseUrl": "https:\/\/events.wixapps.net\/_api\/wix-one-events-server",
                            "staticsBaseUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0"
                        },
                        "errorReportingUrl": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748",
                        "widgets": {
                            "14d2abc2-5350-6322-487d-8c16ff833c8a": {
                                "controllerUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/details-pageController.bundle.min.js",
                                "componentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/details-pageViewerWidget.bundle.min.js",
                                "noCssComponentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/details-pageViewerWidgetNoCss.bundle.min.js",
                                "errorReportingUrl": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748",
                                "widgetId": "14d2abc2-5350-6322-487d-8c16ff833c8a"
                            },
                            "1440e92d-47d8-69be-ade7-e6de40127106": {
                                "controllerUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/widgetController.bundle.min.js",
                                "componentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/widgetViewerWidget.bundle.min.js",
                                "noCssComponentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/widgetViewerWidgetNoCss.bundle.min.js",
                                "errorReportingUrl": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748",
                                "widgetId": "1440e92d-47d8-69be-ade7-e6de40127106"
                            },
                            "405eb115-a694-4e2b-abaa-e4762808bb93": {
                                "controllerUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/members-pageController.bundle.min.js",
                                "componentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/members-pageViewerWidget.bundle.min.js",
                                "noCssComponentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/members-pageViewerWidgetNoCss.bundle.min.js",
                                "errorReportingUrl": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748",
                                "widgetId": "405eb115-a694-4e2b-abaa-e4762808bb93"
                            },
                            "29ad290c-8529-4204-8fcf-41ef46e0d3b0": {
                                "controllerUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/scheduleController.bundle.min.js",
                                "componentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/scheduleViewerWidget.bundle.min.js",
                                "noCssComponentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/scheduleViewerWidgetNoCss.bundle.min.js",
                                "errorReportingUrl": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748",
                                "widgetId": "29ad290c-8529-4204-8fcf-41ef46e0d3b0"
                            }
                        }
                    },
                    "dataBinding": {
                        "appDefId": "dataBinding",
                        "appDefName": "Data Binding",
                        "viewerScriptUrl": "https:\/\/static.parastorage.com\/services\/dbsm-viewer-app\/1.5340.0\/app.js",
                        "baseUrls": {},
                        "widgets": {}
                    },
                    "675bbcef-18d8-41f5-800e-131ec9e08762": {
                        "appDefId": "675bbcef-18d8-41f5-800e-131ec9e08762",
                        "viewerScriptUrl": "https:\/\/static.parastorage.com\/services\/wix-code-viewer-app\/1.1479.539\/app.js",
                        "baseUrls": {},
                        "widgets": {}
                    }
                },
                "blocksBootstrapData": {
                    "blocksAppsData": {},
                    "experimentsQueryParams": "wix-data-as-namespace=true&add-globals-amd-dependency=true&analyze-imported-namespaces=false&support-generate-web-method-url-override=true"
                },
                "window": {
                    "csrfToken": ""
                },
                "location": {
                    "externalBaseUrl": "https:\/\/andreayjuanra.wixsite.com\/nuestraboda",
                    "isPremiumDomain": false,
                    "metaSiteId": "c8c0aea0-02a2-4cec-b797-48ff289b51dd",
                    "userFileDomainUrl": "filesusr.com"
                },
                "bi": {
                    "ownerId": "7124a860-2eef-41f1-9ff5-671349e48f2a",
                    "isMobileFriendly": true,
                    "isPreview": false
                },
                "platformAPIData": {},
                "wixCodeBootstrapData": {
                    "wixCodeAppDefinitionId": "675bbcef-18d8-41f5-800e-131ec9e08762",
                    "wixCodeInstanceId": "c8c540f1-f9df-45ef-aaa6-fe6b24361508",
                    "wixCloudBaseDomain": "wix-code.com",
                    "dbsmViewerApp": "https:\/\/static.parastorage.com\/services\/dbsm-viewer-app\/1.5340.0",
                    "wixCodePlatformBaseUrl": "https:\/\/static.parastorage.com\/services\/wix-code-platform\/1.1097.2",
                    "wixCodeModel": {
                        "appData": {
                            "codeAppId": "de3bc19c-add4-41a0-ac6f-8e46f4fd2eaf"
                        },
                        "signedAppRenderInfo": "4a37e8ad6c909250ab636b45218b85e4b1e3fa29.eyJncmlkQXBwSWQiOiJkZTNiYzE5Yy1hZGQ0LTQxYTAtYWM2Zi04ZTQ2ZjRmZDJlYWYiLCJodG1sU2l0ZUlkIjoiZDA3MTMwY2UtZWRmMC00YjgyLWEyMmYtY2ZiM2QzNDE0YmQwIiwiZGVtb0lkIjpudWxsLCJzaWduRGF0ZSI6MTY2MTI2MjI4MzEzOH0="
                    },
                    "wixCodePageIds": {},
                    "elementorySupport": {
                        "baseUrl": "https:\/\/andreayjuanra.wixsite.com\/nuestraboda\/_api\/wix-code-public-dispatcher-ng\/siteview"
                    },
                    "codePackagesData": []
                },
                "disabledPlatformApps": {},
                "widgetNames": {
                    "14ce1214-b278-a7e4-1373-00cebd1bef7c": {
                        "appDefinitionId": "14ce1214-b278-a7e4-1373-00cebd1bef7c",
                        "widgets": {}
                    },
                    "140603ad-af8d-84a5-2c80-a0f60cb47351": {
                        "appDefinitionId": "140603ad-af8d-84a5-2c80-a0f60cb47351",
                        "widgets": {
                            "14d2abc2-5350-6322-487d-8c16ff833c8a": "events",
                            "1440e92d-47d8-69be-ade7-e6de40127106": "wix_events",
                            "405eb115-a694-4e2b-abaa-e4762808bb93": "events_members_page",
                            "29ad290c-8529-4204-8fcf-41ef46e0d3b0": "agenda-page"
                        }
                    },
                    "675bbcef-18d8-41f5-800e-131ec9e08762": {
                        "appDefinitionId": "675bbcef-18d8-41f5-800e-131ec9e08762",
                        "widgets": {}
                    },
                    "dataBinding": {
                        "appDefinitionId": "dataBinding",
                        "widgets": {}
                    }
                },
                "essentials": {
                    "appsConductedExperiments": {
                        "140603ad-af8d-84a5-2c80-a0f60cb47351": {
                            "specs.events.ui.ShowCouponInputInPaymentStep": "true",
                            "specs.events.ui.SelectEventsManually": "false",
                            "specs.events.ui.NewWidgetSettingsGFPP": "display",
                            "specs.events.ui.SplitRegButtonDefaults": "false",
                            "specs.events.ui.MyEventsNewName": "display",
                            "specs.events.ui.UseInfraCurrencyFormatter": "true",
                            "specs.events.ui.WUTImage": "true",
                            "specs.events.ui.EventsLoadable": "true",
                            "specs.events.ui.ImageSettings": "true",
                            "specs.events.ui.RsvpButtonSettings": "false",
                            "specs.events.ui.SEOScheduleURL": "true",
                            "specs.events.ui.UseWarmupData": "true",
                            "specs.events.ui.PaidPlansV2Viewer": "false",
                            "specs.events.ui.DefaultSettingsInClient": "true"
                        },
                        "14ce1214-b278-a7e4-1373-00cebd1bef7c": {
                            "specs.forms.AutofillOnSubmit": "false",
                            "specs.crm.FormsViewerPaymentsVerboseContent": "false",
                            "specs.crm.FormsViewerEnhanceAnalytics": "true"
                        }
                    }
                }
            },
            "appsScripts": {
                "urls": {
                    "140603ad-af8d-84a5-2c80-a0f60cb47351": [
                        "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/viewerScript.bundle.min.js",
                        "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/widgetController.bundle.min.js"
                    ]
                },
                "scope": "page"
            },
            "debug": {
                "disablePlatform": false,
                "disableSnapshots": false,
                "enableSnapshots": false
            }
        }
    },
    "siteAssets": {
        "dataFixersParams": {
            "experiments": {
                "dm_migrateToTextTheme": true,
                "bv_remove_add_chat_viewer_fixer": "new"
            },
            "dfVersion": "1.1581.0",
            "isHttps": true,
            "isUrlMigrated": true,
            "metaSiteId": "c8c0aea0-02a2-4cec-b797-48ff289b51dd",
            "quickActionsMenuEnabled": false,
            "siteId": "d07130ce-edf0-4b82-a22f-cfb3d3414bd0",
            "siteRevision": 109,
            "v": 3,
            "cacheVersions": {
                "dataFixer": 6
            }
        },
        "modulesParams": {
            "features": {
                "moduleName": "thunderbolt-features",
                "contentType": "application\/json",
                "resourceType": "features",
                "languageResolutionMethod": "QueryParam",
                "isMultilingualEnabled": false,
                "externalBaseUrl": "https:\/\/andreayjuanra.wixsite.com\/nuestraboda",
                "useSandboxInHTMLComp": true
            },
            "platform": {
                "moduleName": "thunderbolt-platform",
                "contentType": "application\/json",
                "resourceType": "platform",
                "externalBaseUrl": "https:\/\/andreayjuanra.wixsite.com\/nuestraboda"
            },
            "css": {
                "moduleName": "thunderbolt-css",
                "contentType": "application\/json",
                "resourceType": "css",
                "stylableMetadataURLs": [
                    "editor-elements-library.4fd016c02775a13fa6a1ffbd70d1a9894b830c79",
                    "editor-elements-design-systems.c32f9e1091da8c0ce86251dea9badeb71d182248"
                ],
                "ooiVersions": "MTQ0MGU5MmQtNDdkOC02OWJlLWFkZTctZTZkZTQwMTI3MTA2PXAuZXZlbnRzLXZpZXdlci8xLjE3MTIuMC93aWRnZXRWaWV3ZXJXaWRnZXROb0Nzcy5jc3NDb25maWcuYnVuZGxlLm1pbi5qczsxNGQyYWJjMi01MzUwLTYzMjItNDg3ZC04YzE2ZmY4MzNjOGE9cC5ldmVudHMtdmlld2VyLzEuMTcxMi4wL2RldGFpbHMtcGFnZVZpZXdlcldpZGdldE5vQ3NzLmNzc0NvbmZpZy5idW5kbGUubWluLmpzOzI5YWQyOTBjLTg1MjktNDIwNC04ZmNmLTQxZWY0NmUwZDNiMD1wLmV2ZW50cy12aWV3ZXIvMS4xNzEyLjAvc2NoZWR1bGVWaWV3ZXJXaWRnZXROb0Nzcy5jc3NDb25maWcuYnVuZGxlLm1pbi5qcw=="
            },
            "siteMap": {
                "moduleName": "thunderbolt-site-map",
                "contentType": "application\/json",
                "resourceType": "siteMap"
            }
        },
        "clientTopology": {
            "mediaRootUrl": "https:\/\/static.wixstatic.com",
            "staticMediaUrl": "https:\/\/static.wixstatic.com\/media",
            "moduleRepoUrl": "https:\/\/static.parastorage.com\/unpkg",
            "fileRepoUrl": "https:\/\/static.parastorage.com\/services",
            "siteAssetsUrl": "https:\/\/siteassets.parastorage.com",
            "pageJsonServerUrls": [
                "https:\/\/pages.wixstatic.com",
                "https:\/\/staticorigin.wixstatic.com",
                "https:\/\/fallback.wix.com\/wix-html-editor-pages-webapp\/page"
            ],
            "pathOfTBModulesInFileRepoForFallback": "wix-thunderbolt\/dist\/"
        },
        "siteScopeParams": {
            "rendererType": null,
            "wixCodePageIds": [],
            "hasTPAWorkerOnSite": false,
            "viewMode": "desktop",
            "freemiumBanner": true,
            "coBrandingBanner": false,
            "mobileActionsMenu": false,
            "isWixSite": false,
            "urlFormatModel": {
                "format": "slash",
                "forbiddenPageUriSEOs": [
                    "app",
                    "apps",
                    "_api",
                    "robots.txt",
                    "sitemap.xml",
                    "feed.xml",
                    "sites"
                ],
                "pageIdToResolvedUriSEO": {}
            },
            "pageJsonFileNames": {
                "v7e3t": "7124a8_a8a7321381a414eb057304249f995668_54.json",
                "c1dmp": "7124a8_203e279d4025c48b4bc127b1171c21cf_109.json",
                "warpj": "7124a8_e3130bce56f4404bce7192350f2160a5_88.json",
                "masterPage": "7124a8_74362e978f8557817f366e24afa79bd2_108.json"
            },
            "protectedPageIds": [],
            "routersInfo": {},
            "anonymousClientSpecMap": {
                "4": {
                    "type": "public",
                    "applicationId": 4,
                    "appDefinitionId": "14d7032a-0a65-5270-cca7-30f599708fed",
                    "appDefinitionName": "WixCoupons",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {
                        "nonDiscoverable": true
                    }
                },
                "5": {
                    "type": "public",
                    "applicationId": 5,
                    "appDefinitionId": "14bca956-e09f-f4d6-14d7-466cb3f09103",
                    "appDefinitionName": "Wix Cashier",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {
                        "isStandalone": true,
                        "semanticVersion": "^0.3.0"
                    }
                },
                "6": {
                    "type": "public",
                    "applicationId": 6,
                    "appDefinitionId": "141fbfae-511e-6817-c9f0-48993a7547d1",
                    "appDefinitionName": "Inbox",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {}
                },
                "7": {
                    "type": "public",
                    "applicationId": 7,
                    "appDefinitionId": "13ee94c1-b635-8505-3391-97919052c16f",
                    "appDefinitionName": "Wix Invoices",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": null
                },
                "8": {
                    "type": "public",
                    "applicationId": 8,
                    "appDefinitionId": "150ae7ee-c74a-eecd-d3d7-2112895b988a",
                    "appDefinitionName": "Marketing Integration",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {}
                },
                "9": {
                    "type": "public",
                    "applicationId": 9,
                    "appDefinitionId": "a322993b-2c74-426f-bbb8-444db73d0d1b",
                    "appDefinitionName": "One App",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {},
                    "version": "0.0.26"
                },
                "10": {
                    "type": "public",
                    "applicationId": 10,
                    "appDefinitionId": "55cd9036-36bb-480b-8ddc-afda3cb2eb8d",
                    "appDefinitionName": "PriceQuotes",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {}
                },
                "11": {
                    "type": "public",
                    "applicationId": 11,
                    "appDefinitionId": "f123e8f1-4350-4c9b-b269-04adfadda977",
                    "appDefinitionName": "Promote Home",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {}
                },
                "12": {
                    "type": "public",
                    "applicationId": 12,
                    "appDefinitionId": "9bead16f-1c73-4cda-b6c4-28cff46988db",
                    "appDefinitionName": "Facebook Ads",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {}
                },
                "13": {
                    "type": "sitemembers",
                    "applicationId": 13,
                    "collectionType": "Open",
                    "collectionFormFace": "Register",
                    "collectionExposure": "Public",
                    "smcollectionId": "837e82b7-c0d3-454c-9462-e7e03c0103e9",
                    "instanceId": "",
                    "instance": ""
                },
                "14": {
                    "type": "public",
                    "applicationId": 14,
                    "appDefinitionId": "1480c568-5cbd-9392-5604-1148f5faffa0",
                    "appDefinitionName": "Get Found on Google",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {}
                },
                "15": {
                    "type": "public",
                    "applicationId": 15,
                    "appDefinitionId": "13aa9735-aa50-4bdb-877c-0bb46804bd71",
                    "appDefinitionName": "Promote SEO Patterns",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {}
                },
                "16": {
                    "type": "public",
                    "applicationId": 16,
                    "appDefinitionId": "e3118e0a-b1c1-4e1d-b67d-ddf0cb92309b",
                    "appDefinitionName": "Promote VideoMaker Home",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {}
                },
                "18": {
                    "type": "public",
                    "applicationId": 18,
                    "appDefinitionId": "14b89688-9b25-5214-d1cb-a3fb9683618b",
                    "appDefinitionName": "Mobile App-Social Posts",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {}
                },
                "19": {
                    "type": "public",
                    "applicationId": 19,
                    "appDefinitionId": "135c3d92-0fea-1f9d-2ba5-2a1dfb04297e",
                    "appDefinitionName": "Email Marketing",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {
                        "141995eb-c700-8487-6366-a482f7432e2b": {
                            "widgetUrl": "https:\/\/so-feed.codev.wixapps.net\/widget",
                            "widgetId": "141995eb-c700-8487-6366-a482f7432e2b",
                            "refreshOnWidthChange": true,
                            "mobileUrl": "https:\/\/so-feed.codev.wixapps.net\/widget",
                            "published": true,
                            "mobilePublished": true,
                            "seoEnabled": true,
                            "preFetch": false,
                            "shouldBeStretchedByDefault": false,
                            "shouldBeStretchedByDefaultMobile": false,
                            "componentFields": {},
                            "tpaWidgetId": "shoutout_feed",
                            "default": true
                        }
                    },
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {
                        "premiumBundle": {
                            "parentAppId": "ee21fe60-48c5-45e9-95f4-6ca8f9b1c9d9",
                            "parentAppSlug": "ee21fe60-48c5-45e9-95f4-6ca8f9b1c9d9"
                        },
                        "isStandalone": true,
                        "semanticVersion": "^0.5.0"
                    }
                },
                "20": {
                    "type": "public",
                    "applicationId": 20,
                    "appDefinitionId": "146c0d71-352e-4464-9a03-2e868aabe7b9",
                    "appDefinitionName": "Ascend Tasks",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {}
                },
                "21": {
                    "type": "public",
                    "applicationId": 21,
                    "appDefinitionId": "139ef4fa-c108-8f9a-c7be-d5f492a2c939",
                    "appDefinitionName": "Automated Emails",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": null
                },
                "22": {
                    "type": "public",
                    "applicationId": 22,
                    "appDefinitionId": "ea2821fc-7d97-40a9-9f75-772f29178430",
                    "appDefinitionName": "Workflows",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {}
                },
                "24": {
                    "type": "public",
                    "applicationId": 24,
                    "appDefinitionId": "307ba931-689c-4b55-bb1d-6a382bad9222",
                    "appDefinitionName": "Video Maker",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {}
                },
                "25": {
                    "type": "public",
                    "applicationId": 25,
                    "appDefinitionId": "d70b68e2-8d77-4e0c-9c00-c292d6e0025e",
                    "appDefinitionName": "Promote SEO Tools",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {}
                },
                "26": {
                    "type": "public",
                    "applicationId": 26,
                    "appDefinitionId": "4b10fcce-732d-4be3-9d46-801d271acda9",
                    "appDefinitionName": "Secrets Vault",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {},
                    "version": "0.0.30"
                },
                "27": {
                    "type": "public",
                    "applicationId": 27,
                    "appDefinitionId": "14ce1214-b278-a7e4-1373-00cebd1bef7c",
                    "appDefinitionName": "Wix Forms & Payments",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {
                        "platform": {
                            "editorScriptUrl": "https:\/\/static.parastorage.com\/services\/wix-form-builder\/1.5499.0\/editor-app.bundle.min.js",
                            "viewerScriptUrl": "https:\/\/static.parastorage.com\/services\/forms-viewer\/1.773.0\/viewer-app.bundle.min.js",
                            "hasDashboardComponent": true,
                            "baseUrls": {
                                "": ""
                            },
                            "margins": {
                                "desktop": {
                                    "top": {
                                        "type": "PX",
                                        "value": 0
                                    },
                                    "right": {
                                        "type": "PX",
                                        "value": 0
                                    },
                                    "bottom": {
                                        "type": "PX",
                                        "value": 0
                                    },
                                    "left": {
                                        "type": "PX",
                                        "value": 0
                                    }
                                },
                                "tablet": {
                                    "top": {},
                                    "right": {},
                                    "bottom": {},
                                    "left": {}
                                },
                                "mobile": {
                                    "top": {},
                                    "right": {},
                                    "bottom": {},
                                    "left": {}
                                }
                            },
                            "height": {
                                "desktop": {},
                                "tablet": {},
                                "mobile": {}
                            },
                            "viewerScriptUrlTemplate": "https:\/\/static.parastorage.com\/services\/forms-viewer\/1.773.0\/viewer-app.bundle.min.js",
                            "isStretched": {},
                            "docking": {
                                "desktop": {
                                    "horizontal": "HCENTER",
                                    "vertical": "TOP_DOCKING"
                                },
                                "tablet": {},
                                "mobile": {}
                            },
                            "errorReporting": {},
                            "width": {
                                "desktop": {},
                                "tablet": {},
                                "mobile": {}
                            },
                            "viewer": {
                                "errorReporting": {}
                            }
                        },
                        "premiumBundle": {
                            "parentAppId": "ee21fe60-48c5-45e9-95f4-6ca8f9b1c9d9",
                            "parentAppSlug": "ee21fe60-48c5-45e9-95f4-6ca8f9b1c9d9"
                        },
                        "mostPopularPackage": "Mid_Range",
                        "featuresForNewPackagePicker": [
                            {
                                "forPackages": [
                                    {
                                        "value": "Unlimited",
                                        "packageId": "Basic"
                                    },
                                    {
                                        "value": "Unlimited",
                                        "packageId": "Mid_Range"
                                    },
                                    {
                                        "value": "Unlimited",
                                        "packageId": "Pro"
                                    }
                                ]
                            },
                            {
                                "forPackages": [
                                    {
                                        "value": "true",
                                        "packageId": "Basic"
                                    },
                                    {
                                        "value": "true",
                                        "packageId": "Mid_Range"
                                    },
                                    {
                                        "value": "true",
                                        "packageId": "Pro"
                                    }
                                ]
                            },
                            {
                                "forPackages": [
                                    {
                                        "value": "100MB Storage",
                                        "packageId": "Basic"
                                    },
                                    {
                                        "value": "1GB Storage",
                                        "packageId": "Mid_Range"
                                    },
                                    {
                                        "value": "10GB Storage",
                                        "packageId": "Pro"
                                    }
                                ]
                            },
                            {
                                "forPackages": [
                                    {
                                        "value": "10 Forms",
                                        "packageId": "Basic"
                                    },
                                    {
                                        "value": "100 Forms",
                                        "packageId": "Mid_Range"
                                    },
                                    {
                                        "value": "Unlimited",
                                        "packageId": "Pro"
                                    }
                                ]
                            },
                            {
                                "forPackages": [
                                    {
                                        "value": "1000 Submissions\/month",
                                        "packageId": "Basic"
                                    },
                                    {
                                        "value": "5000 Submissions\/month",
                                        "packageId": "Mid_Range"
                                    },
                                    {
                                        "value": "Unlimited",
                                        "packageId": "Pro"
                                    }
                                ]
                            },
                            {
                                "forPackages": [
                                    {
                                        "value": "1 User",
                                        "packageId": "Basic"
                                    },
                                    {
                                        "value": "5 Users",
                                        "packageId": "Mid_Range"
                                    },
                                    {
                                        "value": "Unlimited Users",
                                        "packageId": "Pro"
                                    }
                                ]
                            },
                            {
                                "forPackages": [
                                    {
                                        "value": "true",
                                        "packageId": "Basic"
                                    },
                                    {
                                        "value": "true",
                                        "packageId": "Mid_Range"
                                    },
                                    {
                                        "value": "true",
                                        "packageId": "Pro"
                                    }
                                ]
                            },
                            {
                                "forPackages": [
                                    {
                                        "value": "true",
                                        "packageId": "Basic"
                                    },
                                    {
                                        "value": "true",
                                        "packageId": "Mid_Range"
                                    },
                                    {
                                        "value": "true",
                                        "packageId": "Pro"
                                    }
                                ]
                            },
                            {
                                "forPackages": [
                                    {
                                        "value": "true",
                                        "packageId": "Basic"
                                    },
                                    {
                                        "value": "true",
                                        "packageId": "Mid_Range"
                                    },
                                    {
                                        "value": "true",
                                        "packageId": "Pro"
                                    }
                                ]
                            },
                            {
                                "forPackages": [
                                    {
                                        "value": "true",
                                        "packageId": "Basic"
                                    },
                                    {
                                        "value": "true",
                                        "packageId": "Mid_Range"
                                    },
                                    {
                                        "value": "true",
                                        "packageId": "Pro"
                                    }
                                ]
                            },
                            {
                                "forPackages": [
                                    {
                                        "value": "true",
                                        "packageId": "Basic"
                                    },
                                    {
                                        "value": "true",
                                        "packageId": "Mid_Range"
                                    },
                                    {
                                        "value": "true",
                                        "packageId": "Pro"
                                    }
                                ]
                            },
                            {
                                "forPackages": [
                                    {
                                        "value": "true",
                                        "packageId": "Basic"
                                    },
                                    {
                                        "value": "true",
                                        "packageId": "Mid_Range"
                                    },
                                    {
                                        "value": "true",
                                        "packageId": "Pro"
                                    }
                                ]
                            },
                            {
                                "forPackages": [
                                    {
                                        "value": "true",
                                        "packageId": "Mid_Range"
                                    },
                                    {
                                        "value": "true",
                                        "packageId": "Pro"
                                    }
                                ]
                            }
                        ],
                        "isStandalone": true,
                        "semanticVersion": "^0.395.0"
                    }
                },
                "28": {
                    "type": "public",
                    "applicationId": 28,
                    "appDefinitionId": "8725b255-2aa2-4a53-b76d-7d3c363aaeea",
                    "appDefinitionName": "Subscriptions",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {}
                },
                "29": {
                    "type": "public",
                    "applicationId": 29,
                    "appDefinitionId": "8ea9df15-9ff6-4acf-bbb8-8d3a69ae5841",
                    "appDefinitionName": "Financial Settings",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {},
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {}
                },
                "1969": {
                    "type": "public",
                    "applicationId": 1969,
                    "appDefinitionId": "140603ad-af8d-84a5-2c80-a0f60cb47351",
                    "appDefinitionName": "Wix Events",
                    "instance": "",
                    "instanceId": "",
                    "sectionPublished": true,
                    "sectionMobilePublished": false,
                    "sectionSeoEnabled": true,
                    "widgets": {
                        "14d2abc2-5350-6322-487d-8c16ff833c8a": {
                            "widgetUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/details-page",
                            "widgetId": "14d2abc2-5350-6322-487d-8c16ff833c8a",
                            "refreshOnWidthChange": true,
                            "mobileUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/details-page",
                            "appPage": {
                                "id": "events",
                                "name": "Event Details",
                                "defaultPage": "",
                                "hidden": true,
                                "multiInstanceEnabled": false,
                                "order": 1,
                                "indexable": true,
                                "fullPage": false,
                                "landingPageInMobile": false,
                                "hideFromMenu": false
                            },
                            "published": true,
                            "mobilePublished": true,
                            "seoEnabled": true,
                            "preFetch": false,
                            "shouldBeStretchedByDefault": true,
                            "shouldBeStretchedByDefaultMobile": false,
                            "componentFields": {
                                "useSsrSeo": true,
                                "controllerUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/details-pageController.bundle.min.js",
                                "subPages": [
                                    {
                                        "key": "wix.events.sub_pages.event",
                                        "enumerable": true
                                    }
                                ],
                                "componentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/details-pageViewerWidget.bundle.min.js",
                                "viewer": {
                                    "errorReporting": {
                                        "url": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748"
                                    }
                                },
                                "isLoadable": true,
                                "ooiInEditor": true,
                                "noCssComponentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/details-pageViewerWidgetNoCss.bundle.min.js"
                            },
                            "default": false
                        },
                        "1440e92d-47d8-69be-ade7-e6de40127106": {
                            "widgetUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/widget",
                            "widgetId": "1440e92d-47d8-69be-ade7-e6de40127106",
                            "refreshOnWidthChange": true,
                            "mobileUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/widget",
                            "published": true,
                            "mobilePublished": true,
                            "seoEnabled": true,
                            "preFetch": false,
                            "shouldBeStretchedByDefault": false,
                            "shouldBeStretchedByDefaultMobile": false,
                            "componentFields": {
                                "useSsrSeo": true,
                                "componentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/widgetViewerWidget.bundle.min.js",
                                "controllerUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/widgetController.bundle.min.js",
                                "viewer": {
                                    "errorReporting": {
                                        "url": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748"
                                    }
                                },
                                "mobileHelpId": "a0621ef7-79ef-4a32-a376-1258506a5d2a",
                                "mobileSettingsEnabled": true,
                                "ooiInEditor": true,
                                "noCssComponentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/widgetViewerWidgetNoCss.bundle.min.js"
                            },
                            "tpaWidgetId": "wix_events",
                            "default": true
                        },
                        "405eb115-a694-4e2b-abaa-e4762808bb93": {
                            "widgetUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/members-page",
                            "widgetId": "405eb115-a694-4e2b-abaa-e4762808bb93",
                            "refreshOnWidthChange": true,
                            "mobileUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/members-page",
                            "appPage": {
                                "id": "events_members_page",
                                "name": "Events",
                                "defaultPage": "",
                                "hidden": true,
                                "multiInstanceEnabled": false,
                                "order": 2,
                                "indexable": true,
                                "fullPage": false,
                                "landingPageInMobile": false,
                                "hideFromMenu": false
                            },
                            "published": true,
                            "mobilePublished": true,
                            "seoEnabled": false,
                            "preFetch": false,
                            "shouldBeStretchedByDefault": false,
                            "shouldBeStretchedByDefaultMobile": true,
                            "componentFields": {
                                "componentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/members-pageViewerWidget.bundle.min.js",
                                "controllerUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/members-pageController.bundle.min.js",
                                "viewer": {
                                    "errorReporting": {
                                        "url": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748"
                                    }
                                },
                                "ooiInEditor": true,
                                "noCssComponentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/members-pageViewerWidgetNoCss.bundle.min.js"
                            },
                            "default": false
                        },
                        "29ad290c-8529-4204-8fcf-41ef46e0d3b0": {
                            "widgetUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/schedule",
                            "widgetId": "29ad290c-8529-4204-8fcf-41ef46e0d3b0",
                            "refreshOnWidthChange": true,
                            "mobileUrl": "https:\/\/editor.wixapps.net\/render\/prod\/editor\/events-viewer\/1.1712.0\/schedule",
                            "appPage": {
                                "id": "Schedule",
                                "name": "Schedule",
                                "defaultPage": "",
                                "hidden": true,
                                "multiInstanceEnabled": false,
                                "order": 1,
                                "indexable": true,
                                "fullPage": false,
                                "landingPageInMobile": false,
                                "hideFromMenu": false
                            },
                            "published": true,
                            "mobilePublished": true,
                            "seoEnabled": false,
                            "shouldBeStretchedByDefault": true,
                            "shouldBeStretchedByDefaultMobile": true,
                            "componentFields": {
                                "controllerUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/scheduleController.bundle.min.js",
                                "componentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/scheduleViewerWidget.bundle.min.js",
                                "viewer": {
                                    "errorReporting": {
                                        "url": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748"
                                    }
                                },
                                "ooiInEditor": true,
                                "noCssComponentUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/scheduleViewerWidgetNoCss.bundle.min.js"
                            },
                            "tpaWidgetId": "agenda-page",
                            "default": false
                        }
                    },
                    "appRequirements": {
                        "requireSiteMembers": false
                    },
                    "isWixTPA": true,
                    "installedAtDashboard": true,
                    "permissions": {
                        "revoked": false
                    },
                    "appFields": {
                        "platform": {
                            "baseUrls": {
                                "baseUrl": "https:\/\/events.wixapps.net\/_api\/wix-one-events-server",
                                "staticsBaseUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0"
                            },
                            "editorScriptUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/editorScript.bundle.min.js",
                            "viewerScriptUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0\/viewerScript.bundle.min.js",
                            "baseUrlsTemplate": {
                                "staticsBaseUrl": "https:\/\/static.parastorage.com\/services\/events-viewer\/1.1712.0"
                            },
                            "margins": {
                                "desktop": {
                                    "top": {},
                                    "right": {},
                                    "bottom": {},
                                    "left": {}
                                },
                                "tablet": {
                                    "top": {},
                                    "right": {},
                                    "bottom": {},
                                    "left": {}
                                },
                                "mobile": {
                                    "top": {},
                                    "right": {},
                                    "bottom": {},
                                    "left": {}
                                }
                            },
                            "height": {
                                "desktop": {},
                                "tablet": {},
                                "mobile": {}
                            },
                            "isStretched": {},
                            "docking": {
                                "desktop": {},
                                "tablet": {},
                                "mobile": {}
                            },
                            "errorReporting": {
                                "url": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748"
                            },
                            "width": {
                                "desktop": {},
                                "tablet": {},
                                "mobile": {}
                            },
                            "viewer": {
                                "errorReporting": {
                                    "url": "https:\/\/f1ffc0b5efe04e9eb9762cd808722520@sentry.wixpress.com\/748"
                                }
                            },
                            "ooiInEditor": true
                        },
                        "appConfig": {
                            "siteConfig": {
                                "siteStructureApi": "wixArtifactId:com.wixpress.wix-events-web"
                            },
                            "namespace": "wix.events"
                        },
                        "isStandalone": true,
                        "semanticVersion": "^0.727.0"
                    }
                },
                "2256": {
                    "type": "siteextension",
                    "applicationId": 2256,
                    "appDefinitionId": "675bbcef-18d8-41f5-800e-131ec9e08762",
                    "instance": "",
                    "instanceId": ""
                },
                "-666": {
                    "type": "metasite",
                    "metaSiteId": "c8c0aea0-02a2-4cec-b797-48ff289b51dd",
                    "appDefId": "22bef345-3c5b-4c18-b782-74d4085112ff",
                    "instance": "",
                    "appDefinitionId": "22bef345-3c5b-4c18-b782-74d4085112ff",
                    "applicationId": -666,
                    "instanceId": ""
                }
            },
            "isPremiumDomain": false,
            "disableSiteAssetsCache": false,
            "migratingToOoiWidgetIds": "",
            "siteRevisionConfig": {},
            "registryLibrariesTopology": [
                {
                    "artifactId": "editor-elements",
                    "namespace": "wixui",
                    "url": "https:\/\/static.parastorage.com\/services\/editor-elements\/1.9440.0"
                },
                {
                    "artifactId": "editor-elements",
                    "namespace": "dsgnsys",
                    "url": "https:\/\/static.parastorage.com\/services\/editor-elements\/1.9440.0"
                }
            ],
            "isInSeo": false,
            "excludedSafariOrIOS": false,
            "language": "es",
            "originalLanguage": "en",
            "appDefinitionIdToSiteRevision": {}
        },
        "beckyExperiments": {
            "specs.thunderbolt.responsiveAbsoluteChildrenPosition": true,
            "specs.thunderbolt.containersList": true,
            "specs.thunderbolt.DatePickerPortal": true,
            "specs.thunderbolt.SearchBoxSuggestionsFacelift": true,
            "specs.thunderbolt.loadHighQualityImagesAboveFold": true,
            "specs.thunderbolt.loadFirstNFonts": "3",
            "specs.thunderbolt.maskImageCSS": true,
            "specs.thunderbolt.chat_landing_page": true
        },
        "manifests": {
            "node": {
                "modulesToHashes": {
                    "thunderbolt-features": "f7c2bb42.bundle.min",
                    "thunderbolt-platform": "b14f8e32.bundle.min",
                    "thunderbolt-platform-simple": "deb843ed.bundle.min",
                    "thunderbolt-css": "dd4798f6.bundle.min",
                    "thunderbolt-site-map": "d668f3be.bundle.min"
                }
            },
            "web": {
                "modulesToHashes": {
                    "thunderbolt-features": "b61ebb7d.bundle.min",
                    "thunderbolt-platform": "a8c2dc86.bundle.min",
                    "thunderbolt-platform-simple": "607eee6a.bundle.min",
                    "thunderbolt-css": "09699478.bundle.min",
                    "thunderbolt-site-map": "2f012699.bundle.min",
                    "webpack-runtime": "12cefe7c.bundle.min"
                },
                "webpackRuntimeBundle": "12cefe7c.bundle.min"
            },
            "webWorker": {
                "modulesToHashes": {
                    "thunderbolt-features": "8c8d9506.bundle.min",
                    "thunderbolt-platform": "cbc64941.bundle.min",
                    "thunderbolt-platform-simple": "6f646db4.bundle.min",
                    "thunderbolt-css": "e36cb1fa.bundle.min",
                    "thunderbolt-site-map": "ae2affcf.bundle.min"
                }
            }
        },
        "staticHTMLComponentUrl": "https:\/\/andreayjuanra-wixsite-com.filesusr.com\/",
        "remoteWidgetStructureBuilderVersion": "1.233.0"
    },
    "experiments": {
        "specs.thunderbolt.WixCodeAddGlobalsAmdDependency": true,
        "storeSanpshotOnRedis": true,
        "specs.thunderbolt.remove_comp_events_navigation_guard": true,
        "specs.thunderbolt.responsiveAbsoluteChildrenPosition": true,
        "specs.thunderbolt.WixCodeTelemetryNewEndpoint": true,
        "specs.thunderbolt.exposeSendSetPasswordInVelo": true,
        "specs.thunderbolt.dataCancelLinkPreventDefaultRemoval": true,
        "specs.thunderbolt.WixCodeSupportGenerateWebMethodUrlOverride": true,
        "specs.wixData.DynamicPagesRouter": true,
        "specs.thunderbolt.newLoginFlowOnProtectedCollection": true,
        "specs.thunderbolt.increasePlatformBiEventsExposure": true,
        "specs.thunderbolt.containersList": true,
        "specs.thunderbolt.byRefV2": true,
        "specs.thunderbolt.DatePickerPortal": true,
        "specs.thunderbolt.WixCodeAsyncDispatcher": true,
        "specs.thunderbolt.SearchBoxSuggestionsFacelift": true,
        "dm_migrateToTextTheme": true,
        "specs.thunderbolt.TagsCustomConfig": true,
        "specs.thunderbolt.loadHighQualityImagesAboveFold": true,
        "specs.thunderbolt.rejectApprovalNeededAuthenticationRequests": true,
        "specs.thunderbolt.shouldLoadGoogleSdkEarly": true,
        "specs.thunderbolt.checkIOSToAvoidBeacon": true,
        "specs.thunderbolt.loadFirstNFonts": "3",
        "specs.thunderbolt.veloWixMembersAmbassadorV2": true,
        "specs.thunderbolt.exposeNewDashboardSdkWithVelo": true,
        "specs.thunderbolt.blocksPlatformSDKViewer": true,
        "specs.thunderbolt.wixCodePrefetch": true,
        "specs.router.ShouldSupportMultilingualBySubdirectory": true,
        "specs.thunderbolt.allow_windows_reduced_motion": true,
        "specs.thunderbolt.siteAssetsPrefetch": true,
        "displayWixAdsNewVersion": true,
        "specs.thunderbolt.smModalsShouldWaitForAppDidMount": true,
        "specs.thunderbolt.ooi_css_optimization": true,
        "specs.thunderbolt.veloWixMembers": true,
        "specs.ShouldForceCaptchaVerificationOnSignupSpec": "Enabled",
        "specs.thunderbolt.FilterResponsiveEditorMasterPageTpas": true,
        "specs.thunderbolt.enableClientSideRenderTrailingHeader": true,
        "specs.thunderbolt.HangingSignupForNeededApprovalFlow": true,
        "specs.thunderbolt.fetchPriority": true,
        "specs.thunderbolt.maskImageCSS": true,
        "specs.thunderbolt.WixDataNamespace": true,
        "specs.thunderbolt.newAuthorizedPagesFlow": true,
        "specs.thunderbolt.chat_landing_page": true,
        "specs.thunderbolt.resourceFetcherConformToFetchApi": true
    },
    "fleetConfig": {
        "fleetName": "thunderbolt-renderer-light-gradual",
        "type": "Rollout",
        "code": 1
    },
    "dynamicModelUrl": "https:\/\/andreayjuanra.wixsite.com\/nuestraboda\/_api\/v2\/dynamicmodel",
    "rollout": {
        "siteAssetsVersionsRollout": false,
        "isDACRollout": 0,
        "isTBRollout": true
    },
    "commonConfig": {
        "brand": "wix",
        "bsi": "",
        "consentPolicy": {},
        "consentPolicyHeader": {}
    },
    "componentsLibrariesTopology": [
        {
            "artifactId": "editor-elements",
            "namespace": "wixui",
            "url": "https:\/\/static.parastorage.com\/services\/editor-elements\/1.9440.0"
        },
        {
            "artifactId": "editor-elements",
            "namespace": "dsgnsys",
            "url": "https:\/\/static.parastorage.com\/services\/editor-elements\/1.9440.0"
        }
    ],
    "anywhereConfig": {}
}